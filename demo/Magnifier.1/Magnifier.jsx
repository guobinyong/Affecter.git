//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem,computeLoopItemTypeCreater} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Magnifier.css';

import img2 from '../assets/img2.png';








class ItemContent extends Component {
    constructor(props){
        super(props);
        this.itemIndex = props.itemIndex;
    }

    componentWillUnmount(){
        console.log("item卸载:",this.props.itemIndex);
    }

    shouldComponentUpdate(nextProps, nextState){
       return this.itemIndex != nextProps.itemIndex ;
    }

    render() {
        let {style,itemData} = this.props;
        return (
            <div >
                <img className="img" src={img2}/>
                <p>{itemData}</p>
            </div>
        );
    }

}

let ComputeLoopItem = computeLoopItemTypeCreater(ItemContent);


// 组件
class Magnifier extends Component {
    constructor(props) {
        super(props);




        this.getItemInitStyle = this.getItemInitStyle.bind(this);
        this.clickHandle = this.clickHandle.bind(this);

        let newAnchor = this.creatAffectAnchor();
        let affectAnchors = [newAnchor];
        this.state = {
            affectAnchors:affectAnchors,
            total:50
        };


        this.areaAffecterData = {
            areaAffecter:{
                width:400,
                height:600
            },
            affectedItem:{
                width:50,
                height:50,
                space:20
            }
        };

        let {width:areaAffecterWidth,height:areaAffecterHeight} = this.areaAffecterData.areaAffecter;
        let {width:affectedItemWidth,height:affectedItemHeight,space} = this.areaAffecterData.affectedItem;

        this.areaAffecterStyle = {
            width:`${areaAffecterWidth}px`,
            height:`${areaAffecterHeight}px`
        };

        this.affectedItemStyle = {
            width:`${affectedItemWidth}px`,
            height:`${affectedItemHeight}px`
        };

        this.maxRowNum = parseInt(areaAffecterHeight/(affectedItemHeight + space)) * 2;






        let effectRang = {
            x:areaAffecterWidth/2,
            y:areaAffecterHeight/2
        };

        //循环计算
        // this.transforms = [Transforms.hypotenuse,Transforms.magnifierCreater(effectRang,effectRang)];
        let itemDataArr = [];

        for (let index = 0;index < 50;index++){
            itemDataArr.push(index);
        }

        this.itemDataArr = itemDataArr;

        //css布局
        this.transforms = [Transforms.hypotenuse,Transforms.magnifierCreater({x:150,y:150})];
    }





    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        console.log("影响样式回调：")
        let allx = distanceArr.map(function(distance,index,arr){
            return distance.x;
        });
        let maxx = Math.max(...allx);
        let translateZ = maxx * 200;
        return {transform:`perspective(300px) translateZ(${translateZ}px)`};
    }

    //初始样式
    getItemInitStyle(element,index,computeAffectStyleForItem){

        let rowNum = index % this.maxRowNum;
        let colNum = parseInt(index / this.maxRowNum );

        let affectedItem = this.areaAffecterData.affectedItem
        let leftSpace = affectedItem.space * colNum;
        let left = affectedItem.width * colNum + leftSpace;

        let topSpace = affectedItem.space * rowNum;
        let top = affectedItem.height * rowNum + topSpace;

        this.affectedItemStyle.left = `${left}px`;
        this.affectedItemStyle.top = `${top}px`;


        let itemRect = affectedItem;
        itemRect.x = left;
        itemRect.y = top;

        let affectStyle = computeAffectStyleForItem(null,itemRect);

        let initStyle = {...this.affectedItemStyle,...affectStyle};

        return initStyle;
    }

    clickHandle(event){
        let newAnchor = this.creatAffectAnchor();
        let affectAnchors = [newAnchor];
        // this.setState({
        //     affectAnchors:affectAnchors,
        //     // total:this.state.total + 1
        // });

        console.log("点击了");
    }

    creatAffectAnchor(){
        let random = Math.random;
        return {x:random(),y:random()};
    }

    render() {

        let affectAnchors = [
            {x:0.2,y:0.2},
            {x:0.9,y:0.2},
            {x:0.5,y:0.5},
            {x:0.2,y:0.9},
            {x:0.9,y:0.9},
        ];

        // //计算布局
        //
        // return (
        //     <div>
        //         <button onClick={this.clickHandle}>更改影响点</button>
        //         <Affecter  className="affecter" style={this.areaAffecterStyle}  affectAnchors={this.state.affectAnchors} getItemInitStyle={this.getItemInitStyle}  getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}>
        //             {
        //                 function(total){
        //                     let arr = [];
        //                     for (let i = 1;i <= total;i++) {
        //                         let elem = <AffectedItem key={i} className="affected-item">
        //                             <img className={"img"} src={img2}/>
        //                         </AffectedItem> ;
        //                         arr.push(elem);
        //                     }
        //                     return arr;
        //                 }(1000)
        //             }
        //         </Affecter>
        //     </div>
        //
        // );



        //计算布局：循环

        // return (
        //     <div>
        //         <button onClick={this.clickHandle}>更改影响点</button>
        //         <Affecter  className="affecter" loopType="All" style={this.areaAffecterStyle}  affectAnchors={this.state.affectAnchors} getItemInitStyle={this.getItemInitStyle}  getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}>
        //             {
        //                 function(total){
        //                     let arr = [];
        //                     for (let i = 1;i <= total;i++) {
        //                         let elem = <AffectedItem key={i} renderAll={true} className="affected-item">
        //                             <img className={"img"} src={img2}/>
        //                         </AffectedItem> ;
        //                         arr.push(elem);
        //                     }
        //                     return arr;
        //                 }(1000)
        //             }
        //         </Affecter>
        //     </div>
        //
        // );


        // css布局
        // return (
        //     <div>
        //         <button onClick={this.clickHandle}>更改影响点</button>
        //         <Affecter  className="affecter" wrapChildren={true} loopType="All"   affectAnchors={[{x:0.5,y:0.5}]}   getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}>
        //             {
        //                 function(total){
        //                     let arr = [];
        //                     for (let i = 1;i <= total;i++) {
        //                         let elem = <AffectedItem key={i} className="affected-item">
        //                             <img className={"img"} src={img2}/>
        //                         </AffectedItem> ;
        //                         arr.push(elem);
        //                     }
        //                     return arr;
        //                 }(300)
        //             }
        //         </Affecter>
        //     </div>
        // );





        // 循环
        // return (
        //     <div>
        //         <button onClick={this.clickHandle}>更改影响点</button>
        //         <Affecter onClick={this.clickHandle} className="affecter" loopType="All" wrapClass="wrap"   affectAnchors={this.state.affectAnchors}   getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}>
        //             {
        //                 function(total){
        //                     console.log("总数：",total);
        //                     let arr = [];
        //                     for (let i = 1;i <= total;i++) {
        //                         let elem = <AffectedItem key={i} className="affected-item">
        //                             <img className={"img"} src={img2}/>
        //                         </AffectedItem> ;
        //                         arr.push(elem);
        //                     }
        //                     return arr;
        //                 }(this.state.total)
        //             }
        //         </Affecter>
        //     </div>
        // );




        //计算循环
        /*
    ### 计算循环(ComputeLoop)独有的Props
        ItemType
        itemSize :{width,height}
        horSpace
        verSpace
        roundItemCount
        roundRowCount
        roundColCount
        getItemData : (itemIndex:number,row:number,col:number)=>Data

            */

        return (
            <div>
                <button onClick={this.clickHandle}>更改影响点</button>
                <Affecter onClick={this.clickHandle} className="affecter"  throttleDelay={0} throttleStep={0} ItemType={ComputeLoopItem} itemSize={{width:50,height:50}} horSpace={20} verSpace={20} roundRowCount={5}  renderExtendRadius={100}  itemDataArr={this.itemDataArr} loopType="All" affectAnchors={this.state.affectAnchors}   getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms}></Affecter>
            </div>
        );

    }
}











//导出
export {Magnifier};

