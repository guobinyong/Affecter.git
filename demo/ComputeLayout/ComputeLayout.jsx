//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './ComputeLayout.css';

import img2 from '../assets/img2.png';




// 有转换、有影响
class ComputeLayout extends Component {

    constructor(props){
        super(props);

        this.transforms = [Transforms.abs,Transforms.magnifierCreater({x:150,y:150})];
    }


    // 初始样式
    getItemInitStyle(element,index,computeAffectStyleForItem){

        let maxRowNum = 5;
        let space = 20;
        let itemSize = 50;

        
        let rowNum = parseInt(index / maxRowNum );
        let colNum = index % maxRowNum;

        let leftSpace = space * colNum;
        let left = itemSize * colNum + leftSpace;

        let topSpace = space * rowNum;
        let top = itemSize * rowNum + topSpace;

        let itemRect = {x:left,y:top,width:itemSize,height:itemSize};

        let itemStyle = {left:`${left}px`,top:`${top}px`};   //通过 left、top 来定位；
        let affectStyle = computeAffectStyleForItem(null,itemRect);     //传入item 的矩形，获取该item的影响样式
        let initStyle = {...itemStyle,...affectStyle};    // 合并所有的样式

        return initStyle;       //返回合并后的样式
    }


    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let dis = distanceArr[0];
        let magn = 150 * dis.y;
        return {transform:`perspective(500px) translateZ(${magn}px)`};
    }

    render() {

        let itemArr = [];
        for (let i = 1; i <= 300; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }


        return (
            <Affecter  className="affecter"   affectAnchors={[{x:0.5,y:0.5}]} transforms={this.transforms} getItemAffectStyle={this.getItemAffectStyle} getItemInitStyle={this.getItemInitStyle}  >{itemArr}</Affecter>
        );

    }
}











/* 
// 无影响、无转换
class ComputeLayout extends Component {


    // 初始样式
    getItemInitStyle(element,index,computeAffectStyleForItem){

        let maxRowNum = 5;
        let space = 20;
        let itemSize = 50;

        
        let rowNum = parseInt(index / maxRowNum );
        let colNum = index % maxRowNum;

        let leftSpace = space * colNum;
        let left = itemSize * colNum + leftSpace;

        let topSpace = space * rowNum;
        let top = itemSize * rowNum + topSpace;

        let itemRect = {x:left,y:top,width:itemSize,height:itemSize};

        let itemStyle = {left:`${left}px`,top:`${top}px`};   //通过 left、top 来定位；

        return itemStyle;
    }


    render() {

        let itemArr = [];
        for (let i = 1; i <= 300; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }


        return (
            <Affecter  className="affecter"   affectAnchors={[{x:0.5,y:0.5}]}  getItemInitStyle={this.getItemInitStyle}  >{itemArr}</Affecter>
        );

    }
} */




/* 
// 无 computeAffectStyleForItem 
class ComputeLayout extends Component {

    constructor(props){
        super(props);

        this.transforms = [Transforms.abs,Transforms.magnifierCreater({x:150,y:150})];
    }


    // 初始样式
    getItemInitStyle(element,index,computeAffectStyleForItem){

        let maxRowNum = 5;
        let space = 20;
        let itemSize = 50;

        
        let rowNum = parseInt(index / maxRowNum );
        let colNum = index % maxRowNum;

        let leftSpace = space * colNum;
        let left = itemSize * colNum + leftSpace;

        let topSpace = space * rowNum;
        let top = itemSize * rowNum + topSpace;

        let itemRect = {x:left,y:top,width:itemSize,height:itemSize};

        let itemStyle = {left:`${left}px`,top:`${top}px`};   //通过 left、top 来定位；

        return itemStyle;
    }


    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let dis = distanceArr[0];
        let magn = 150 * dis.y;
        return {transform:`perspective(500px) translateZ(${magn}px)`};
    }

    render() {

        let itemArr = [];
        for (let i = 1; i <= 300; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }


        return (
            <Affecter  className="affecter"   affectAnchors={[{x:0.5,y:0.5}]} transforms={this.transforms} getItemAffectStyle={this.getItemAffectStyle} getItemInitStyle={this.getItemInitStyle}  >{itemArr}</Affecter>
        );

    }
}
 */




//导出
export {ComputeLayout};
