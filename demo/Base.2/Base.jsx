//导入
import React, { Component } from 'react';



import { Affecter, AffectedItem } from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Base.css';

import img2 from '../assets/img2.png';




// 组件
class Base extends Component {

    render() {

        return (
            <Affecter className="affecter" >
                <AffectedItem className="affected-item" key="1" >
                    <img className="img" src={img2} />
                </AffectedItem>

                <AffectedItem className="affected-item"  key="2" >
                    <img className="img" src={img2} />
                </AffectedItem>

                <AffectedItem className="affected-item" key="3" >
                    <img className="img" src={img2} />
                </AffectedItem>
            </Affecter>
        );

    }
}





//导出
export { Base };