//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Magnifier.css';

import img2 from '../assets/img2.png';









// 组件
class Magnifier extends Component {
    constructor(props) {
        super(props);
        this.transforms = [Transforms.hypotenuse,Transforms.magnifierCreater({x:100,y:100})];
    }





    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let scale = distanceArr[0].x;
        let translateZ = scale * 150;
        return {transform:`perspective(400px) translateZ(${translateZ}px)`};
    }




    render() {

        let itemArr = [];
        for (let i = 1; i <= 500; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }

        return (
            <Affecter className="affecter" affectAnchors={[{x:0.5,y:0.5}]} wrapChildren={true} wrapClass="wrap" getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms} >{itemArr}</Affecter>
        );

    }
}






//导出
export {Magnifier};

