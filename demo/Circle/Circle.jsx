//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Circle.css';

import img2 from '../assets/img2.png';




// 组件
class Circle extends Component {

    constructor(props){
        super(props);

        this.transforms = [Transforms.rightAngleEdgeCreater({x:300,y:300},0)];
    }


    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let dis = distanceArr[0];
        let znum = (dis.y - 300) * 3;
        return {transform:`perspective(100px) translateZ(${znum}px)`};

        let magn = dis.y/200;
        return {transform:`scale(${magn})`};

        
    }

    render() {

        let itemArr = [];
        for (let i = 1; i <= 300; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }


        // css布局
        return (
            <Affecter  className="affecter" wrapChildren={true}   affectAnchors={[{x:0.5,y:0.5}]} transforms={this.transforms} getItemAffectStyle={this.getItemAffectStyle}  >{itemArr}</Affecter>
        );

    }
}











//导出
export {Circle};

