//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Magnifier.css';

import img2 from '../assets/img2.png';










// 转换器示例代码

class Magnifier extends Component {
    constructor(props) {
        super(props);
        this.transforms = [this.abs, this.magnifierTransform];
    }



    // 绝对值转换器
    abs(distance) {
        let abs = Math.abs;

        let transformedResult = {
            x: abs(distance.x),
            y: abs(distance.y)
        };      //转换的距离坐标

        let newDistance = { ...distance, ...transformedResult };    //覆盖原来距离坐标对象中的 x 和 y ，但保留原来坐标对象中的其它数据；
        return newDistance;     //返回合并后的距离坐标
    }


    // 放大镜转换器
    magnifierTransform(distance) {

        let referencerX = 100;
        let referencerY = 100;

        let rangeX = Number.POSITIVE_INFINITY;
        let rangeY = Number.POSITIVE_INFINITY;

        let abs = Math.abs;
        let resultX = distance.x;
        let resultY = distance.y;

        let effectResultX = abs(resultX) < rangeX ? resultX : rangeX;
        let effectResultY = abs(resultY) < rangeY ? resultY : rangeY;

        let magnifierX = 1 - (effectResultX / referencerX);
        let magnifiery = 1 - (effectResultY / referencerY);

        let transformedResult = { x: magnifierX, y: magnifiery };   //转换的距离坐标

        let newDistance = { ...distance, ...transformedResult };    //覆盖原来距离坐标对象中的 x 和 y ，但保留原来坐标对象中的其它数据；
        return newDistance;    //返回合并后的距离坐标
    }






    //影响样式
    getItemAffectStyle(distanceArr, itemElement, containerElement, itemRowCol, itemRect) {
        let scale = distanceArr[0].y;
        let translateZ = scale * 150;
        return { transform: `perspective(400px) translateZ(${translateZ}px)` };
    }


    render() {

        let itemArr = [];
        for (let i = 1; i <= 200; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }

        return (
            // 给 Affecter 传入转换器数据
            <Affecter className="affecter" affectAnchors={[{ x: 0.5, y: 0.5 }]} wrapChildren={true} wrapClass="wrap" getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms} >{itemArr}</Affecter>
        );

    }
}

//导出
export {Magnifier};
