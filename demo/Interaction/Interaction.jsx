//导入
import React,{Component} from 'react';



import {Affecter,AffectedItem} from '../../affecter/Affecter';
import * as Transforms from '../../affecter/Transforms';


import './Interaction.css';

import img2 from '../assets/img2.png';









// 组件
class Interaction extends Component {
    constructor(props) {
        super(props);
        this.eventHandel = this.eventHandel.bind(this);
        this.transforms = [Transforms.hypotenuse,Transforms.magnifierCreater({x:100,y:100})];

        this.state = {affectAnchor:{x:0,y:0}};
        
    }





    //影响样式
    getItemAffectStyle(distanceArr,itemElement,containerElement,itemRowCol,itemRect){
        let scale = distanceArr[0].x;
        let translateZ = scale * 150;
        return {transform:`perspective(400px) translateZ(${translateZ}px)`};
    }


    eventHandel(event){
        this.setState({affectAnchor:{x:event.clientX,y:event.clientY}});
    }

    scrollHandel(event){
        return true;
    }




    render() {

        let itemArr = [];
        for (let i = 1; i <= 200; i++) {
            let elem = (
                <AffectedItem key={i} className="affected-item">
                    <img className={"img"} src={img2} />
                </AffectedItem>
            );
            itemArr.push(elem);
        }

        return (
            <Affecter   onMouseOver={this.eventHandel}   affectAnchors={[this.state.affectAnchor]}   usePixelCoordInAffecter={true}  className="affecter"   wrapChildren={true} wrapClass="wrap" getItemAffectStyle={this.getItemAffectStyle} transforms={this.transforms} >{itemArr}</Affecter>
        );

    }
}






//导出
export {Interaction};

