
[本项目的Git仓库]: https://gitee.com/guobinyong/Affecter.git



目录
======
```
一、简介
二、安装方式
三、API
```


内容
======



# 一、简介
rect-rel 是一个关于点、矩形关系的函数库，里面有各种关于矩形关系计算的函数；

**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe





# 二、安装方式
目前，安装方式有以下作几种：

## 方式1：直接下载原代码
您可直接从 [本项目的Git仓库][] 下载，然后直接把 [本项目的Git仓库][] 下的 rect-pack/RectRelation.js 文件拷贝到您的项目中去，并通过如下代码在您的项目中引入您需要使用的矩形关系函数工具：
```
import { 您需要的函数,您需要的函数 } from "path/to/RectRelation.js";
```

## 方式2：通过 npm 安装
```
npm install --save rect-rel
```


# 三、API

## 矩形的包含判断
`rectIsContain(outRect,innerRect)`
- @param outRect :{x:number,y:number,width:number,height:number}   包含者
- @param innerRect :{x:number,y:number,width:number,height:number}   被包含者
- @returns : boolean   表示包含者是否包含被包含者

**注意：**  
包含及相等都属于包含；




-------

## 矩形的相交或包含判断
`rectIsCrossOrContain(rect1,rect2)`
- @param rect1 :{x:number,y:number,width:number,height:number}   矩形1
- @param rect2 :{x:number,y:number,width:number,height:number}   矩形2
- @returns : boolean   表示矩形1与矩形2是否相交或包含

**注意：**  
不包含相切；




-------

## 矩形水平方向上的相交或包含判断

`rectIsCrossOrContainOnHor(rect1,rect2)`
- @param rect1 :{x:number,y:number,width:number,height:number}   矩形1
- @param rect2 :{x:number,y:number,width:number,height:number}   矩形2
- @returns : boolean   表示矩形1与矩形2在水平方向上是否相交或包含

**注意：**  
不包含相切；



-------
 
## 矩形垂直方向上的相交或包含判断
`rectIsCrossOrContainOnVer(rect1,rect2)`
- @param rect1 :{x:number,y:number,width:number,height:number}   矩形1
- @param rect2 :{x:number,y:number,width:number,height:number}   矩形2
- @returns : boolean   表示矩形1与矩形2在垂直方向上是否相交或包含

**注意: **  
不包含相切;




-------
 
## 判断在水平方向上 frontRect 是否在 backRect 的前面，且不相交
`rectIsInFrontOnHor(frontRect,backRect)`
- @param frontRect :{x:number,y:number,width:number,height:number}   前面的矩形
- @param backRect :{x:number,y:number,width:number,height:number}   后面的矩形
- @returns : boolean   表示在水平方向上 frontRect 是否在 backRect 的前面，且不相交；

**注意：**  
包含相切




-------
 
## 判断在垂直方向上 frontRect 是否在 backRect 的前面，且不相交
`rectIsInFrontOnVer(frontRect,backRect)`
- @param frontRect :{x:number,y:number,width:number,height:number}   前面的矩形
- @param backRect :{x:number,y:number,width:number,height:number}   后面的矩形
- @returns : boolean   表示在垂直方向上 frontRect 是否在 backRect 的前面，且不相交；

**注意：**  
包含相切






-------
 
## 矩形的相交判断
`rectIsCross(rect1,rect2)`
- @param rect1 :{x:number,y:number,width:number,height:number}   矩形1
- @param rect2 :{x:number,y:number,width:number,height:number}   矩形2
- @returns : boolean   表示矩形1与矩形2是否相交

**注意：**  
包含不属于相交




-------

## 矩形的相切
`rectIstTangent(rect1,rect2)`
- @param rect1 :{x:number,y:number,width:number,height:number}   矩形1
- @param rect2 :{x:number,y:number,width:number,height:number}   矩形2
- @returns : boolean   表示矩形1与矩形2是否相切




-------
 
## 判断点是否在矩形内
`pointInRect(point,rect)`
- @param point :{x:number,y:number}   点
- @param rect :{x:number,y:number,width:number,height:number}   矩形
- @returns : boolean

**注意：**  
不包含 点 在 矩形边上的情况



-------
 
## 判断点是否在矩形上
`pointOnRect(point,rect)`
- @param point :{x:number,y:number}   点
- @param rect :{x:number,y:number,width:number,height:number}   矩形
- @returns : boolean

**注意：**  
包含 点 在 矩形边上的情况






-------
 
## 判断点是否在矩形的边框上
`pointOnRectBorder(point,rect)`
- @param point :{x:number,y:number}   点
- @param rect :{x:number,y:number,width:number,height:number}   矩形
- @returns : boolean   表示是否在矩形的边框上




-------

## 扩展矩形
`extendRect(rect,horExtendRadius,verExtendRadius)`
- @param rect :{x:number,y:number,width:number,height:number} 原矩形
- @param horExtendRadius : number 水平扩展半径
- @param verExtendRadius : number 垂直扩展半径
- @returns : {x: number, y: number, width: number, height: number}  扩展后的矩形
