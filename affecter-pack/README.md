
[本项目的Git仓库]: https://gitee.com/guobinyong/Affecter.git
[Affecter的教程]: https://gitee.com/guobinyong/Affecter
[Affecter的接口文档]: https://gitee.com/guobinyong/Affecter/blob/master/doc/Affecter的接口文档.md
[内置Transforms的接口文档]: https://gitee.com/guobinyong/Affecter/blob/master/doc/内置Transforms的接口文档.md




# 一、简介
`Affecter` 中文名字是 `影响者`，它用于根据子元素与其与特定1个或者多位置的距离对子元素设置样式;

关于 Affecter 的详细信息，请参考以下文档：
- [Affecter的教程][]
- [Affecter的接口文档][]
- [内置Transforms的接口文档][]




**如果您在使用该库的过程中有遇到了问题，或者有好的建议和想法，您都可以通过以下方式联系我，期待与您的交流：**  
- 邮箱：guobinyong@qq.com
- QQ：guobinyong@qq.com
- 微信：keyanzhe





# 二、安装方式
```
npm install --save affecter
```
