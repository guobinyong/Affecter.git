Affecter的接口文档
=================



# 概念
- Affecter 影响者 : 用于根据子元素与其与特定1个或者多位置的距离对子元素设置样式;
- AffectedItem 影响项目 : Affecter 所展示的内容的基本单位，也是被影响的对象；
- affectAnchor 影响锚点 : 影响 item 的点；
- itemAnchor 项目锚点 : 项目的锚点；



# 类型表示规则
```
ScaleNumber : 表示比例的数字类型；
Coord = {x:number,y:number}      : 表示坐标的类型 
Size = {width:number,height:number}     : 表示尺寸的类型
Rect = {x:number,y:number,width:number,height:number}       : 表示矩形的类型
Element : 原生Dom元素
RowCol = {row:number,col:number}    : 表示行列号的类型
Style : React样式对象类型
CalssName : React的calssName的类型
```




Affecter 影响者
========================


# props
## affectAnchors
- 类型: [Coord]?

### 说明
affectAnchors中的元素表示影响锚点在Affecter的视口上的偏移坐标；
坐标的单位由 `usePixelCoordInAffecter` 决定；


## usePixelCoordInAffecter
- 类型: boolean?

### 说明
表示affectAnchors中的坐标单位是否是像素；
当为 true 时，affectAnchors 中坐标单位是像素；
当为 false 或者未定义时，affectAnchors 中坐标单位是偏移像素相对于 Affecter 宽度和高度的比例； 即： x 表示 影响锚点在X轴方向上相对于Affecter的宽度的偏移比例，y 表示 影响锚点在Y轴方向上相对于Affecter的高度的偏移比例；



## itemAnchor 项目锚点
- 类型: {x:ScaleNumber,y:ScaleNumber}
- 默认值: {x:0.5,y:0.5}

### 说明
表示 项目锚点 在 项目视口上的 偏移坐标，用于计算与影响锚点之间的距离；单位是：偏移像素相对于项目宽高的偏移比例；



## transforms
- 类型: [ (distance:Coord,index:number,distanceArr:[Coord],itemRect :Rect,itemElement?:Element,containerElement?:Element)=>Coord ]?

### 说明
转换函数数组，里面包含的函数是用来转换项目元素的锚点与影响锚点之间的距离坐标，并返回转换后的距离坐标对象；



## getItemAffectStyle
- 类型: (distanceArr : [Coord],itemElement : Element?,containerElement : Element?,itemRowCol:{row:number,col:number,index:number},itemRect:Rect)=>Style

### 说明
获得项目影响样式的回调函数；



## getItemInitStyle
- 类型 : (element,index,computeAffectStyleForItem : (itemElement : Element ?, orItemRect : Rect ?)=>Style )=>Style  ?

### 说明
获得元素的初始样式（比如：用于布局的样式）的回调函数，在Affecter的props更新时为每个元素调用；


## wrapChildren
- 类型: boolean?

### 说明
表示是否包装子元素；




## wrapClass
- 类型: CalssName?

### 说明
定义包装元素的类




## wrapStyle
- 类型: Style?

### 说明
定义包装元素的样式



## loopType
- 类型: "Hor" | "Ver" | "All" | "false"  ?

### 说明
定义循环类型


## wrapSpace
- 类型: number?
- 默认值: 0

### 说明
定义循环包之间的间隔；




## onScroll
- 类型: Function ?

### 说明
Affecter 的滑动事件处理函数；当返回 true 值时，不会执行 Affecter 的默认操作；





计算循环(ComputeLoop)独有的Props
===============

## ItemType
- 类型: ReactComponent

### 说明
计算循环的项目类型，必须是由工厂函数 `computeLoopItemTypeCreater` 创建的类型；



## itemDataArr
- 类型: Array

### 说明
计算循环的项目的数据数组



## itemSize
- 类型: Size
- 说明: 项目的尺寸




## horSpace
- 类型: nunber
- 说明: 项目间的水平间距



## verSpace
- 类型: number
- 说明: 项目间的垂直间距



## itemDataArr
- 类型: Array
- 说明: 项目的数据数组




## roundRowCount
- 类型: number
- 说明: 单个循环周期内的行数；`roundRowCount` 和 `roundColCount` 中只需设置其中一个，当这两个prop都被设置时，会忽略 `roundColCount` 的设置；




## roundColCount
- 类型: number
- 说明: 单个循环周期内的列数；`roundRowCount` 和 `roundColCount` 中只需设置其中一个，当这两个prop都被设置时，会忽略 `roundColCount` 的设置；








AffectedItem 影响者的项目
========================


# props

**以下props是由使用者来定义**  

## renderAll
- 类型: boolean ?

### 说明
每次刷新是否渲染所有元素，默认是否，只渲染 渲染矩形 内的元素；


**以下props由Affecter组件负责传送，用户不用传送**  

## computeAffectStyleForItem
- 类型: (itemElement : Element ?, orItemRect : Rect ?)=>Style
- 说明: 计算元素被影响的样式


## itemIsInRenderRange
- 类型: (itemElement : Element)=>boolean
- 说明: 用于判断该元素是否在渲染矩形内；
### 使用说明
AffectedItem 组件专门用作 Affecter 组件的子组件，使用格式为：
```
<Affecter>
    <AffectedItem>
        <p>郭斌勇</p>
    </AffectedItem>
</Affecter>
```

您也可创建自己的AffectedItem组件，只需要在需要设置样式时通过 this.props.computeAffectStyleForItem 函数获取新的样式即可；






工具函数
===============

## computeLoopItemTypeCreater(ItemContentType)
- @param ItemContentType : Component   组件
- @returns : Component   可以作为计算循环模式下的 Affecter 的 item 的组件类；

### 说明
生成可以作为计算循环模式下的 Affecter 的 item 的组件类； ItemContentType 的实例会收到以下 prop：
- row  当前 item 的行号
- col  当前 item 的列号
- itemIndex    当前 item 的项目序号
- itemData    item 的数据

行号、列号是 计算循环为 项目设计的网络坐标，每个网格对应一个项目的位置；  
项目序号 表示的是项目在计算循环的循环单元（循环周期）内的序号；所以如果2个item的 itemIndex 相同，则它们接收的 itemData 也是相同的；







性能优化相关的props
==================

## throttleDelay
- 类型: number ?
- 默认值: 40

### 说明
节流阀体的时间限，即该数值表示多长时间内不允许触发第2次更新


## throttleStep
- 类型: number ?
- 默认值: 2

### 说明
节流阀体的步长限，即该数值表示多长的滑动距离才能触发第2次更新


## renderExtendRadius
- 类型: number ?

### 说明
项目的渲染范围的扩展半径；

